# osbuild-auto

The [osbuild-auto](https://gitlab.com/CentOS/automotive/rpms/osbuild-auto/) is a collection of python scripts for custom build stages to 
[CentOS Automotive SIG](https://sigs.centos.org/automotive/) project which uses [osbuild](https://www.osbuild.org). The osbuild-auto project is consumed via RPM.

## building and testing the scripts
As soon you have the python script ready copy the file to the osbuild stages dir.

Example:

Copy the new conf file into the stages dir
```bash
cp org.osbuild-auto.initoverlayfs.conf /usr/lib/osbuild/stages/org.osbuild-auto.initoverlayfs.conf 
```

Build a new CentOS Automotive image using the new script
```bash
git clone https://gitlab.com/CentOS/automotive/sample-images && cd sample-images/osbuild-manifests
sample-images/osbuild-manifests# sudo make cs9-qemu-initoverlayfs-ostree.x86_64.qcow2
```
