#!/usr/bin/python3
"""
Change OSTree configuration

Change the configuration for an OSTree repository.
Currently only the following values are supported:
  - `sysroot.readonly`
  - `sysroot.bootloader`

See `ostree.repo-config(5)` for more information.
"""

import os
import subprocess
import sys

import osbuild.api

SCHEMA = """
"additionalProperties": false,
"required": ["repo"],
"properties": {
  "repo": {
    "description": "Location of the OSTree repo.",
    "type": "string"
  },
  "config": {
    "type": "object",
    "additionalProperties": false,
    "description": "OSTree configuration groups",
    "properties": {
      "sysroot": {
        "type": "object",
        "additionalProperties": false,
        "description": "Options concerning the sysroot",
        "properties": {
          "composefs": {
            "description": "Enable composefs image generation on deploy.",
            "type": "string",
            "enum": ["true", "false", "maybe"]
          }
        }
      }
    }
  }
}
"""


def ostree(*args, _input=None, **kwargs):
    args = list(args) + [f'--{k}={v}' for k, v in kwargs.items()]
    print("ostree " + " ".join(args), file=sys.stderr)
    subprocess.run(["ostree"] + args,
                   encoding="utf8",
                   stdout=sys.stderr,
                   input=_input,
                   check=True)


def main(tree, options):
    repo = os.path.join(tree, options["repo"].lstrip("/"))
    sysroot_options = options["config"].get("sysroot", {})

    composefs = sysroot_options.get("composefs")
    if composefs is not None:
        ostree("config", "set", "ex-integrity.composefs", composefs, repo=repo)


if __name__ == '__main__':
    stage_args = osbuild.api.arguments()
    r = main(stage_args["tree"],
             stage_args["options"])
    sys.exit(r)
