#!/usr/bin/python3
"""Enable fs-verity on an ostree repo/deployment

The way osbuild works the ostree deployment is built in a chroot and
stored as a regular directory of files before finally being copied to
the physical filesystem. This means ostree fs-verity support doesn't
work, as the fs-verity setting of files is not copied.

To support fs-verity in generated images you have to run this stage
after copying the final ostree tree onto the target filesystem.

Note: Ensure the target filesystem supports fs-verity. See e.g.  the
`verity` option in org.ostbuild.mkfs.ext4.

"""

import fcntl
import os
import struct
import sys

import osbuild.api

SCHEMA_2 = r"""
"options": {
  "additionalProperties": false,
  "required": ["repo"],
  "properties": {
     "repo": {
      "description": "Location of the OSTree repo.",
      "type": "string"
    }
  }
},
"devices": {
  "type": "object",
  "additionalProperties": true
},
"mounts": {
  "type": "array"
}
"""

FS_IOC_ENABLE_VERITY = 0x40806685
FS_VERITY_HASH_ALG_SHA256 = 1


def enable_fsverity(path):
    with open(path, "rb") as f:
        try:
            fcntl.ioctl(f, FS_IOC_ENABLE_VERITY, struct.pack("IIIIQIIQ11Q",
                                                             1,
                                                             FS_VERITY_HASH_ALG_SHA256,
                                                             4096,
                                                             0, 0,
                                                             0, 0, 0,
                                                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
        except FileExistsError:
            pass  # fsverity already enabled


def ostree_deploy_enable_verity(deploydir):
    cfsfile = os.path.join(deploydir, ".ostree.cfs")
    if os.path.isfile(cfsfile):
        enable_fsverity(cfsfile)


def ostree_repo_enable_verity(repo):
    # Enable fs-verity on all backing objetcs
    for root, _, f_names in os.walk(os.path.join(repo, "objects")):
        for f in f_names:
            p = os.path.join(root, f)
            # Don't fs-verity symlink
            if not os.path.islink(p):
                enable_fsverity(os.path.join(root, f))

    # Enable fs-verity on all deployed composefs images
    topdeploydir = os.path.join(repo, "../deploy")
    with os.scandir(topdeploydir) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                deploydir = os.path.join(topdeploydir, entry.name, "deploy")
                with os.scandir(deploydir) as it:
                    for entry in it:
                        if not entry.name.startswith('.') and entry.is_dir():
                            ostree_deploy_enable_verity(os.path.join(deploydir, entry.name))


def main(args, options):
    root = args["mounts"].get("root", {}).get("path")
    repo = os.path.join(root, options["repo"].lstrip("/"))
    ostree_repo_enable_verity(repo)


if __name__ == '__main__':
    stage_args = osbuild.api.arguments()
    r = main(stage_args,
             stage_args["options"])
    sys.exit(r)
